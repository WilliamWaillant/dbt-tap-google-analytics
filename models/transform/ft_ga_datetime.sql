select 
ga_avg_session_duration::float,
ga_bounces::int,
ga_date::date,
(ga_hour::varchar || ':00')::time,
ga_sessions::int,
ga_unique_pageviews::int,
ga_users::int,
c.id_campaign::int,
dc.id_device_category::int,
m.id_medium::int,
r.id_region::int,
s.id_source::int
from {{var('schema')}}.correspondo_ga_datetime as cor
join {{ref ('campaign_ga')}} c on c.campaign = cor.ga_campaign
join {{ref ('device_category_ga')}} dc on dc.device_category = cor.ga_device_category
join {{ref ('medium_ga')}} m on m.medium = cor.ga_medium
join {{ref ('region_ga')}} r on r.region = cor.ga_region
join {{ref ('source_ga')}} s on s.source = cor.ga_source