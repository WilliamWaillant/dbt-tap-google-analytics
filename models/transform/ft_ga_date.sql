select 
ga_ad_clicks::int,
ga_ad_cost::float,
ga_impressions::int,
ga_organic_searches::int,
ga_avg_session_duration::float,
ga_bounces::int,
ga_date::date,
ga_sessions::int,
ga_unique_pageviews::int,
ga_users::int,
c.id_campaign::int,
dc.id_device_category::int,
m.id_medium::int,
s.id_source::int
from {{var('schema')}}.correspondo_ga_date as cor
join {{ref ('campaign_ga')}} c on c.campaign = cor.ga_campaign
join {{ref ('device_category_ga')}} dc on dc.device_category = cor.ga_device_category
join {{ref ('medium_ga')}} m on m.medium = cor.ga_medium
join {{ref ('source_ga')}} s on s.source = cor.ga_source