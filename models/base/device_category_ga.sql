with tab as (
	select distinct ga_device_category AS device_category
	from {{var('schema')}}.correspondo_ga_datetime
)
select device_category, ROW_NUMBER() OVER (ORDER BY device_category) AS id_device_category
FROM tab
