with tab as (
	select distinct ga_region AS region
	from {{var('schema')}}.correspondo_ga_datetime
)
select region, ROW_NUMBER() OVER (ORDER BY region) AS id_region
FROM tab