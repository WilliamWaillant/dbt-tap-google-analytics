with tab as (
	select distinct ga_campaign AS campaign
	from {{var('schema')}}.correspondo_ga_datetime
)
select campaign, ROW_NUMBER() OVER (ORDER BY campaign) AS id_campaign
FROM tab