with tab as (
	select distinct ga_source AS source
	from {{var('schema')}}.correspondo_ga_datetime
)
select source, ROW_NUMBER() OVER (ORDER BY source) AS id_source
FROM tab