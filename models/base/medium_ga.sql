with tab as (
	select distinct ga_medium AS medium
	from {{var('schema')}}.correspondo_ga_datetime
)
select medium, ROW_NUMBER() OVER (ORDER BY medium) AS id_medium
FROM tab